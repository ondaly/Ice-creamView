﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using IcecreamView;

public class ViewController : MonoBehaviour
{

    public static IC_Controller Instance;
    /// <summary>
    /// UI配置表， 这里使用Resource模式的配置表装载
    /// </summary>
    public IC_Resource_ViewConfig viewConfig;

    /// <summary>
    /// 需要指定UI的Canvas作为父节点
    /// </summary>
    public Transform Canvasteansform;
	
    /// <summary>
    /// view管理器，通常来说项目中有几个Canvas就有一个对应的manager，并且是全局变量，这里方便演示（未来将支持多Canvas）
    /// </summary>
    
    private void Awake()
    {
        Instance = new IC_Controller(viewConfig, Canvasteansform);
    }
}
