﻿using UnityEngine;
using UnityEngine.UI;
using IcecreamView;

public class viewmodule_toview : IC_AbstractModule
{
    public Button toview;
    public bool isClose;

    public int stor;
    public string viewTable;

    public override void OnInitView()
    {
        if (toview != null) {
            toview.onClick.AddListener(()=> {
                if (this.isClose)
                    this.ViewConnector.CloseView();
                ViewController.Instance.OpenView(viewTable , this.stor , true);
                this.ViewConnector.SendEvent(1 , "测试");
            });
        }
    }
}
